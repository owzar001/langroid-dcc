import langroid as lr
from langroid.agent.special import TableChatAgent, TableChatAgentConfig

import pandas as pd

# Import csv file
mydata = pd.read_csv("mydata.csv")

agent = TableChatAgent(
    config = TableChatAgentConfig(
        data = mydata,
    )
)

task = lr.Task(
    agent,
    name = "DataAssistant",
    default_human_response="", 
)

result1 = task.run(
  "What types of cell is in each column?",
  turns=2 
) 
