import langroid.language_models as lm

# Simple query

llm_cfg = lm.OpenAIGPTConfig(
    chat_model=lm.OpenAIChatModel.GPT4_TURBO
)
# use LLM directly
mdl = lm.OpenAIGPT(llm_cfg)

response = mdl.chat("The inverse of a self-adjoint linear operator is self-adjoint")



