# Set API key
Create a text file .env with the following line

```
OPENAI_API_KEY=MYAPIKEYNOQOUTES
```

Make this file readable and writable by only by the owner 

```
chmod 700 .env
```

Alternatively, one can export the key from the UNIX shell (but it is unclear if this is secure)

```
$ export OPENAI_API_KEY=MYAPIKEYNOQOUTES
```

See [https://help.openai.com/en/articles/5112595-best-practices-for-api-key-safety] for additional details

# Toy examples for testing setup on DCC

## Test langroid toy example
```

srun \
    --account biostat \
    --partition biostat \
    --mem 32G \
    apptainer exec \
    /opt/apps/containers/community/owzar001/langroid-dcc.sif \
    /opt/langroid/bin/python3 toyexamples/langroid-toy.py
```

## Test langroid toy example on GPU node
```
srun \
    --account biostat \
    --partition biostat-gpu \
    --gres=gpu \
    --mem 32G \
    apptainer exec \
    --nv \
    /opt/apps/containers/community/owzar001/langroid-dcc.sif \
    /opt/langroid/bin/python3 toyexamples/langroid-toy.py
```

## Test openAI API toy example

```
srun \
    --account biostat \
    --partition biostat \
    --mem 32G \
    apptainer exec \
    /opt/apps/containers/community/owzar001/langroid-dcc.sif \
    /opt/openai/bin/python3 toyexamples/openai-toy.py

```

# OpenAI Deprecated Models
https://stackoverflow.com/questions/77789886/openai-api-error-the-model-text-davinci-003-has-been-deprecated
https://platform.openai.com/docs/deprecations
